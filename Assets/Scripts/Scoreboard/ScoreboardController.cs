using System.IO;
using UnityEngine;

namespace Scoreboard
{
    public class ScoreboardController : MonoBehaviour
    {
        private static readonly string Path = Application.persistentDataPath + "/scoreboard.json";
        private const int MAXScoreboardEntries = 10;
        
        public static void AddEntry(UserHighScore highsScore)
        {
            var highScoreList = GetSavedData();
            var scoreAdded = false;
            for (var i = 0; i < highScoreList.scoreList.Count; i++)
            {
                if (highsScore.totalScore <= highScoreList.scoreList[i].totalScore) continue;
                highScoreList.scoreList.Insert(i,highsScore);
                scoreAdded = true;
                break;
            }

            if (!scoreAdded && highScoreList.scoreList.Count < MAXScoreboardEntries)
            {
                highScoreList.scoreList.Add(highsScore);
            }

            if (highScoreList.scoreList.Count > MAXScoreboardEntries)
            {
                highScoreList.scoreList.RemoveRange(MAXScoreboardEntries, 
                    highScoreList.scoreList.Count - MAXScoreboardEntries);
            }
            SavedScore(highScoreList);
        }

        public static UserHighScoreList GetSavedData()
        {
            if (!File.Exists(Path))
            {
                File.Create(Path).Dispose();
                return new UserHighScoreList();
            }
            using var stream = new StreamReader(Path);
            var json = stream.ReadToEnd();
            return JsonUtility.FromJson<UserHighScoreList>(json);
        }

        private static void SavedScore(UserHighScoreList list)
        {
            using var stream = new StreamWriter(Path);
            var json = JsonUtility.ToJson(list, true);
            stream.Write(json);
        }
    }
}
