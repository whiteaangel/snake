using System;

namespace Scoreboard
{
   [Serializable]
   public class UserHighScore
   {
      public string username;
      public int foodScore;
      public int crystalScore;
      public int totalScore;
   }
}
