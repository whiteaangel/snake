using System;
using System.Collections.Generic;

namespace Scoreboard
{
    [Serializable]
    public class UserHighScoreList
    {
        public List<UserHighScore> scoreList = new List<UserHighScore>();
    }
}
