using System.Collections;
using DG.Tweening;
using Extention;
using GameControllers;
using Snake;
using UnityEngine;
using UnityEngine.UI;

namespace UI
{
    public class UIController : MonoBehaviour
    {
        public SnakeHead sHead;
        public LevelEnd levelEndTrigger;
        
        public Text levelNumberText;
        public Text crystalCountText;
        public Text foodCountText;
        public Text multiplierText;
    
        public GameObject messagePanel;
        public GameObject multiplierPanel;

        public void OnEnable()
        {
            sHead.EAddCrystal += UpdateCrystalText;
            sHead.EAddFood += UpdateFoodText;
            
            sHead.EMessage += SetupMessagePanel;
            levelEndTrigger.EMessage += SetupMessagePanel;
        }

        public void OnDisable()
        {
            sHead.EAddCrystal -= UpdateCrystalText;
            sHead.EAddFood -= UpdateFoodText;
            
            sHead.EMessage -= SetupMessagePanel;
            levelEndTrigger.EMessage -= SetupMessagePanel;
        }

        private void Start()
        {
            DOTween.Init();
            ShowMultiplierPanel();
        }

        private void ShowMultiplierPanel()
        {
            multiplierText.text = "multiplier " + "x" + (PlayerPrefs.GetInt("Multiplier")+1);
            multiplierPanel.transform.DOScale(new Vector3(1, 1, 1), 1f);
            StartCoroutine(DelayHideMultiplierPanel());
        }

        private IEnumerator DelayHideMultiplierPanel()
        {
            yield return new WaitForSeconds(1f);
            multiplierPanel.transform.DOScale(new Vector3(0, 0, 0), 1f);
        }
        
        public void SetupLevelNumber(int number)
        {
            levelNumberText.text = "Level " + number;
        }

        public void PauseGame()
        {
            GameSate.CurrentState = GameSate.State.Pause;
            SetupMessagePanel("Game paused");
        }
        
        private void SetupMessagePanel(string message)
        {
            messagePanel.GetComponent<MessagePanel>().Setup(message, sHead.FoodCount, sHead.TotalCrystalCount);
            messagePanel.transform.DOScale(new Vector3(1, 1, 1), 1);
        }
        
        private void UpdateFoodText(string newText)
        {
            foodCountText.text = newText;
        }

        private void UpdateCrystalText(string newText)
        {
            crystalCountText.text = newText;
        }
    }
}
