using System;
using DG.Tweening;
using Extention;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace UI
{
    public class MessagePanel : MonoBehaviour
    {
        public GameObject closeButton;
        public GameObject restartButton;
        public Text messageText;
        public Text foodCount;
        public Text crystalCount;
        
        public GameObject messagePanel;
        public GameObject winPanel;

        private void Start()
        {
            DOTween.Init();
            var trigger = closeButton.GetComponent<EventTrigger>();
            var entry = new EventTrigger.Entry {eventID = EventTriggerType.PointerClick};
            entry.callback.AddListener((data) => { CloseButtonAction((PointerEventData)data); });
            trigger.triggers.Add(entry);
            closeButton.SetActive(false);
        }

        public void Setup(string message, int food, int crystal)
        {
            messageText.text = message;
            switch (GameSate.CurrentState)
            {
                case GameSate.State.Pause:
                    closeButton.SetActive(true);
                    messagePanel.SetActive(true);
                    break;
                case GameSate.State.Dead:
                    messagePanel.SetActive(true);
                    restartButton.SetActive(true);
                    break;
                case GameSate.State.Win:
                    winPanel.SetActive(true);
                    foodCount.text = food.ToString();
                    crystalCount.text = crystal.ToString();
                    break;
            }
        }

        private void CloseButtonAction(PointerEventData data)
        {
            gameObject.transform.DOScale(new Vector3(0, 0, 0), 1);
            GameSate.CurrentState = GameSate.State.Play;
            closeButton.SetActive(false);
        }
    }
}
