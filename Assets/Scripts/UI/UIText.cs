﻿using UnityEngine;
using UnityEngine.UI;

namespace UI
{
    public class UIText : MonoBehaviour
    {
        public void OnPointerEnter()
        {
            AudioController.PlaySound(Resources.Load("Sounds/Click") as AudioClip);
            gameObject.GetComponent<Text>().fontSize += 20;
        }

        public void OnPointerExit()
        {
            gameObject.GetComponent<Text>().fontSize -= 20;
        }
    }
}
