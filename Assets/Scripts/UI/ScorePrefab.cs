using UnityEngine;
using UnityEngine.UI;

namespace UI
{
    public class ScorePrefab : MonoBehaviour
    {
        public Text usernameText;
        public Text totalScore;
        public Text foodCountText;
        public Text crystalCountText;

        public void Setup(string uName, int tScore, int fCount, int cCount)
        {
            usernameText.text = uName;
            totalScore.text = "Score: " + tScore;
            foodCountText.text = fCount.ToString();
            crystalCountText.text = cCount.ToString();
        }
    }
}
