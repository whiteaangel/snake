using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace UI
{
    public class LevelPrefab : MonoBehaviour
    {
        public Text headerText;

        public void Setup(int levelNumber)
        {
            headerText.text = "Level " + levelNumber;
            var trigger = GetComponent<EventTrigger>();
            var entry = new EventTrigger.Entry {eventID = EventTriggerType.PointerClick};
            entry.callback.AddListener((data) => { OnPointerClickDelegate((PointerEventData)data,levelNumber); });
            trigger.triggers.Add(entry);
        }

        private static void OnPointerClickDelegate(PointerEventData data, int levelNumber)
        {
            PlayerPrefs.DeleteKey("FoodCount");
            PlayerPrefs.DeleteKey("CrystalCount");
            PlayerPrefs.DeleteKey("Multiplier");
            SceneManager.LoadScene(levelNumber);
        }

    }
}
