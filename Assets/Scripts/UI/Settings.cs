using System.Collections;
using UnityEngine;
using UnityEngine.UI;

namespace UI
{
    public class Settings : MonoBehaviour
    {
        public Toggle toggle;
        public Slider slider;
        public Text volumeText;
        private void Start()
        {
            StartCoroutine(Setup());
            toggle.onValueChanged.AddListener(delegate { ToggleValueChanged(toggle); });
            slider.onValueChanged.AddListener(RaiseVolume);
        }

        private IEnumerator Setup()
        {
            yield return new WaitForSeconds(0.2f);
            var sound = PlayerPrefs.GetInt("Sound");
            switch (sound)
            {
                case 0:
                    AudioController.SoundControl(false);
                    toggle.isOn = true;
                    break;
                case 1:
                    AudioController.SoundControl(false);
                    toggle.isOn = true;
                    break;
                case 2:
                    AudioController.SoundControl(true);
                    toggle.isOn = false;
                    break;
            }

            var volume = PlayerPrefs.GetFloat("Volume");
            slider.value = volume;
            AudioController.Source.volume = volume;
        }

        public void ClearScoreBoard()
        {
            
        }

        private void ToggleValueChanged(Toggle change)
        {
            if (toggle.isOn)
            {
                AudioController.SoundControl(false);
                volumeText.text = "Sound on";
                PlayerPrefs.SetInt("Sound",1);
            }
            else
            {
                AudioController.SoundControl(true);
                volumeText.text = "Sound Off";
                PlayerPrefs.SetInt("Sound",2);
            }
            PlayerPrefs.Save();
        }

        private void RaiseVolume(float value)
        {
            AudioController.Source.volume = slider.value;
            PlayerPrefs.SetFloat("Volume", slider.value);
            PlayerPrefs.Save();
        }
    }
}
