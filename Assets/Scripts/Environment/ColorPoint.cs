using Extention;
using Snake;
using UnityEngine;

namespace Environment
{
    public class ColorPoint : MonoBehaviour
    {
        public Colors currentColors = Colors.Blue;

        public void Start()
        {
            ChangeColor.ChangeObjectColor(currentColors.ToString(), gameObject);
        }

        public void OnTriggerEnter(Collider other)
        {
            if (!other.CompareTag("Snake")) return;
            AudioController.PlaySound(Resources.Load("Sounds/ChangeColor") as AudioClip);
            other.GetComponent<SnakeHead>().ChangeSnakeColor(currentColors);
        }
    }
}
