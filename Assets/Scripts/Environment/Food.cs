using Extention;
using UnityEngine;

namespace Environment
{
    public class Food : MonoBehaviour
    {
        public Colors currentColor = Colors.Blue;
        public GameObject mesh;
        public void Start()
        {
            ChangeColor.ChangeObjectColor(currentColor.ToString(), mesh);
        }
    }
}
