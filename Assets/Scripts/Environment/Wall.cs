using Snake;
using UnityEngine;

namespace Environment
{
    public class Wall : MonoBehaviour
    {
        public void OnTriggerEnter(Collider other)
        {
            if (other.CompareTag("Snake"))
                other.GetComponent<SnakeHead>().Death("Died against the wall!");
        }
    }
}
