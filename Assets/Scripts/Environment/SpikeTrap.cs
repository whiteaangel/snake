using Snake;
using UnityEngine;

namespace Environment
{
    public class SpikeTrap : MonoBehaviour
    {
        public void OnTriggerEnter(Collider other)
        {
            if (other.CompareTag("Snake") && !other.GetComponent<SnakeHead>().IsFever)
                other.GetComponent<SnakeHead>().Death("Died against the trap!");
        }
    }
}
