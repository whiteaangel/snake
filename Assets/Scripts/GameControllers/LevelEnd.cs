using Extention;
using Snake;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace GameControllers
{
    public class LevelEnd : MonoBehaviour
    {
        public delegate void Message(string reason);
        public event Message EMessage;
        public void OnTriggerEnter(Collider other)
        {
            if (!other.CompareTag("Snake")) return;
            
            other.GetComponent<SnakeHead>().SaveCurrentScore();
            
            UpdateLevelsCount();
            UpdateMultiplier();
            
            GameSate.CurrentState = GameSate.State.Win;
            EMessage?.Invoke("Congratulations your end level!");
        }

        private static void UpdateLevelsCount()
        {
            var count = PlayerPrefs.GetInt("LevelCount");
            if(SceneManager.GetActiveScene().buildIndex < count)
                return;
            count++;
            PlayerPrefs.SetInt("LevelCount", count);
            PlayerPrefs.Save();
        }

        private static void UpdateMultiplier()
        {
            var multiplier = PlayerPrefs.GetInt("Multiplier");
            multiplier++;
            PlayerPrefs.SetInt("Multiplier", multiplier);
            PlayerPrefs.Save();
        }
    }
}
