using UnityEngine;

public class CameraMoving : MonoBehaviour
{
    public Transform player;
    public float offset;

    private void Update()
    {
        if(player == null)
            return;
        var position = transform.position ;
        position.z = player.position.z + offset;
        transform.position = position ;
    }
}
