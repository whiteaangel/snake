using Extention;
using Snake;
using UI;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace GameControllers
{
    public class Controller : MonoBehaviour
    {
        public UIController uiController;
        public SnakeHead sHead;
        public void Start()
        {
            GameSate.CurrentState = GameSate.State.Play;
            uiController.SetupLevelNumber(SceneManager.GetActiveScene().buildIndex);
        }

        public void GoToMenu()
        {
            if (GameSate.CurrentState == GameSate.State.Pause)
            {
                PlayerPrefs.SetInt("FoodCount", sHead.GetComponent<SnakeHead>().FoodCount);
                PlayerPrefs.SetInt("CrystalCount", sHead.GetComponent<SnakeHead>().TotalCrystalCount);
                SnakeHead.UpdateScoreBoard();
            }
            
            PlayerPrefs.DeleteKey("FoodCount");
            PlayerPrefs.DeleteKey("CrystalCount");
            PlayerPrefs.DeleteKey("Multiplier");
            
            SceneManager.LoadScene(0);
        }

        public void GoToNextLevel()
        {
            var sceneIndex = SceneManager.GetActiveScene().buildIndex + 1;
            if(sceneIndex > SceneManager.sceneCountInBuildSettings - 1)
                return;
            SceneManager.LoadScene(sceneIndex);
        }
        
        public void ReloadLevel()
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        }

        public void ExitGame()
        {
            Application.Quit();
        }
    }
}
