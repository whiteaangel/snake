using UnityEngine;

public class AudioController: MonoBehaviour
{
    private static AudioController Instance { get; set; }
    protected  internal static AudioSource Source;

    private void Awake()
    {
        if (Instance)
        {
            DestroyImmediate(gameObject);
            return;
        }
        Instance = this;
        DontDestroyOnLoad(gameObject);
    }

    private void Start()
    {
        Source = GetComponent<AudioSource>();
    }

    public static void StopPlaying()
    {
        Source.Stop();
    }
    
    public static void PlaySound(AudioClip clip)
    {
        Source.PlayOneShot(clip);
    }

    public static void SoundControl(bool isMute)
    {
        Source.mute = isMute;
    }
}
