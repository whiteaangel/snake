using DG.Tweening;
using Scoreboard;
using UI;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace GameControllers
{
   public class StartSceneController : MonoBehaviour
   {
      public GameObject menuPanel;
      public GameObject levelPanel;
      public GameObject settingsPanel;
      public GameObject scorePanel;
      
      public Transform contentTransform;
      public Transform scoreTransform;

      public GameObject levelPrefab;
      public GameObject scorePrefab;
      private void Start()
      {
         DOTween.Init();

         if(PlayerPrefs.GetInt("LevelCount") == 0)
            PlayerPrefs.SetInt("LevelCount", 1);
         
         PlayerPrefs.DeleteKey("FoodCount");
         PlayerPrefs.DeleteKey("CrystalCount");
         PlayerPrefs.DeleteKey("Multiplier");
      }

      public void StartFirstLevel()
      {
         SceneManager.LoadScene(1);
      }

      public void LoadLevels()
      {
         foreach (Transform c in contentTransform)
         {
            Destroy(c.gameObject);
         }
         
         var openLevelCount = PlayerPrefs.GetInt("LevelCount");
         for (var i = 1; i <= openLevelCount; i++)
         {
            var obj = Instantiate(levelPrefab, contentTransform);
            obj.GetComponent<LevelPrefab>().Setup(i);
         }
         OpenAnotherMenu(levelPanel);
      }

      public void LoadSettings()
      {
         OpenAnotherMenu(settingsPanel);
      }

      public void LoadScoreBoard()
      {
         foreach (Transform c in scoreTransform)
         {
            Destroy(c.gameObject);
         }
         
         var highScoreList = ScoreboardController.GetSavedData();
         foreach (var score in highScoreList.scoreList)
         {
            var obj = Instantiate(scorePrefab, scoreTransform);
            obj.GetComponent<ScorePrefab>().Setup(score.username, score.totalScore, score.foodScore,score.crystalScore);
         }
         OpenAnotherMenu(scorePanel);
      }
      
      private void OpenAnotherMenu(GameObject menu)
      {
         menuPanel.transform.DOLocalMove(new Vector3(-menuPanel.GetComponent<RectTransform>().sizeDelta.x,
            0f, 0f), 1f);
         menu.transform.DOLocalMove(new Vector3(0, 0f, 0f), 1f);
      }
      
      public void BackToMenu(GameObject obj)
      {
         menuPanel.transform.DOLocalMove(new Vector3(0, 0f, 0f), 1f);
         obj.transform.DOLocalMove(new Vector3(obj.GetComponent<RectTransform>().sizeDelta.x,
            0f, 0f), 1f);
      }

      public void ExitGame()
      {
         Application.Quit();
      }
   }
}
