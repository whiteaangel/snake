using UnityEngine;

namespace Extention
{
    public static class GameSate
    {
        public enum State
        {
            Pause,
            Play,
            Dead,
            Win
        }

        public static State CurrentState;
    }
}