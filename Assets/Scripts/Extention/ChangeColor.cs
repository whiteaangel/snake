using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class ChangeColor 
{
    public static void ChangeObjectColor(string color, GameObject bodyPart)
    {
        var newMat = Resources.Load("Colors/" + color, typeof(Material)) as Material;
        bodyPart.GetComponent<Renderer>().material = newMat;
    }
}
