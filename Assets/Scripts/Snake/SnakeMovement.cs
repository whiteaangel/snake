using System;
using System.Collections.Generic;
using Unity.Mathematics;
using UnityEngine;

namespace Snake
{
    public class SnakeMovement : MonoBehaviour
    {
        public GameObject firstPart;
        public float minDistance = 0.25f;
        public float speed = 1f;
        public float rotationSpeed = 120f;
    
        protected internal readonly List<Transform> BodyParts = new List<Transform>();
        protected internal bool IsRotateHead;
        private float dis;
        private Transform curBodyPart;
        private Transform prevBodyPart;

        private void Start()
        {
           var multiplier =  PlayerPrefs.GetInt("PassedLevels");
           speed += multiplier;
        }

        public void MoveForward(bool isFever)
        {
            float curSpeed;
            if(!isFever)
                 curSpeed = speed;
            else
            {
                BodyParts[0].position = new Vector3(0, BodyParts[0].position.y, BodyParts[0].position.z);
                curSpeed = speed * 3;
            }
            BodyParts[0].Translate(BodyParts[0].forward * curSpeed * Time.smoothDeltaTime,Space.World);
            MoveTail(curSpeed);
        }

        public void MoveDirection(Touch touch)
        {
            float newSpeed = 0;

            if (touch.position.x < Screen.width / 2 )
            {
                newSpeed = rotationSpeed * -1;
            }
            else if (touch.position.x > Screen.width / 2)
            {
                newSpeed = rotationSpeed;
            }
        
            if(Mathf.Abs(BodyParts[0].transform.rotation.eulerAngles.y) <= 95 || 
               Mathf.Abs(BodyParts[0].transform.rotation.eulerAngles.y) > 300)
                BodyParts[0].transform.Rotate(Vector3.up * newSpeed * Time.smoothDeltaTime);
            if (touch.phase == TouchPhase.Ended)
                IsRotateHead = true;
        }

        public void RotateHeadBack()
        {
            if(BodyParts[0].rotation != quaternion.identity)
                BodyParts[0].rotation = Quaternion.Slerp(BodyParts[0].rotation, Quaternion.identity, 5 * Time.deltaTime);
            else
                IsRotateHead = false;
        }

        private void MoveTail(float currentSpeed)
        {
            for (var i = 1; i < BodyParts.Count; i++)
            {
                curBodyPart = BodyParts[i];
                prevBodyPart = BodyParts[i - 1];
            
                dis = Vector3.Distance(prevBodyPart.position, curBodyPart.position);
                var newPos = prevBodyPart.position;
                newPos.y = BodyParts[0].position.y;

                var T = Time.deltaTime * dis / minDistance * currentSpeed;
                if (T > 0.5f)
                    T = 0.5f;
                curBodyPart.position = Vector3.Slerp(curBodyPart.position, newPos, T);
                curBodyPart.rotation = Quaternion.Slerp(curBodyPart.rotation, prevBodyPart.rotation, T);
            }
        }
    }
}
