using System.Collections;
using Extention;
using UnityEngine;

namespace Snake
{
    public class SnakeController : MonoBehaviour
    {
        public SnakeMovement sMovement;
        public SnakeHead sHead;
        
        public GameObject bodyPrefab;
        public int startSnakeLength = 2 ;
        private void OnEnable()
        {
            sHead.AddPart += AddBodyPart;
            sHead.EChangeColor += ChangeAllSnakeColor;
        }
        
        private void OnDisable()
        {
            sHead.AddPart -= AddBodyPart;
            sHead.EChangeColor -= ChangeAllSnakeColor;
        }

        private void Start()
        {
            sMovement.BodyParts.Add(sMovement.firstPart.transform);
            ChangeColor.ChangeObjectColor(sHead.currentColor.ToString(),sMovement.firstPart.transform.GetChild(0).gameObject);
            ChangeColor.ChangeObjectColor(sHead.currentColor.ToString(),sMovement.firstPart);
            for (var i = 0; i < startSnakeLength - 1; i++)
            {
                AddBodyPart();
            }
        }

        private void Update()
        { 
            if(GameSate.CurrentState != GameSate.State.Play)
                return;
              
            sMovement.MoveForward(sHead.IsFever);
            
            if(sMovement.IsRotateHead)
                sMovement.RotateHeadBack();
            
            if(Input.touchCount <= 0 || sHead.IsFever)
                return;
            var touch = Input.GetTouch(0);
            sMovement.MoveDirection(touch);
        }
        
        private void AddBodyPart()
        {
            var newPart = Instantiate(bodyPrefab, sMovement.BodyParts[sMovement.BodyParts.Count - 1].position,
                sMovement.BodyParts[sMovement.BodyParts.Count - 1].rotation).transform;
            newPart.SetParent(transform);
            ChangeColor.ChangeObjectColor(sHead.currentColor.ToString(),newPart.gameObject);
            sMovement.BodyParts.Add(newPart);
        }
        
        private void ChangeAllSnakeColor(string color)
        {
            var time = 0.1f;
            foreach (var part in sMovement.BodyParts)
            {
                time += 0.1f;
                StartCoroutine(DelayChangeColor(time, color, part.gameObject));
            }
            ChangeColor.ChangeObjectColor(color,sMovement.firstPart.transform.GetChild(0).gameObject);
        }

        private static IEnumerator DelayChangeColor(float time, string color, GameObject part)
        {
            yield return new WaitForSeconds(time);
            ChangeColor.ChangeObjectColor(color, part.gameObject);
        }
    }
}
