using UnityEngine;

namespace Snake
{
    public class SnakeEating : MonoBehaviour
    {
        public SnakeHead snakeHead;
        public void OnTriggerEnter(Collider other)
        {
            if(!snakeHead.IsFever)
                snakeHead.Eating(other);
            else
            {
                snakeHead.FeverEating(other);
            }
        }
    }
}
