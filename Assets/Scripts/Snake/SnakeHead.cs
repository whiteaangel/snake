using System;
using System.Collections;
using Environment;
using Extention;
using Scoreboard;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Snake
{
   public class SnakeHead : MonoBehaviour
   {
      public Colors currentColor = Colors.Blue;
      public ParticleSystem speedEffect;
      
      protected internal int FoodCount;
      
      protected internal int TotalCrystalCount;
      private int currentCrystalCount;
      
      private float timer;
      private bool isStartTimer;
      private int multiplier;
      protected internal bool IsFever;
      
      public event Action AddPart;

      public delegate void ChangeColor(string colorName);
      public event ChangeColor EChangeColor;

      public delegate void Message(string reason);
      public event Message EMessage;
      
      public delegate void AddCrystal(string count);
      public event AddCrystal EAddCrystal;

      public delegate void AddFood(string count);
      public event AddFood EAddFood;

      private void Start()
      {
         multiplier = PlayerPrefs.GetInt("Multiplier");
      }

      private void Update()
      {
         if(!isStartTimer)
            return;
         FeverTimer();
      }
      
      public void Eating(Component other)
      {
         if (other.CompareTag("Food"))
         {
            if (other.GetComponent<Food>().currentColor != currentColor)
            {
               Death("You ate the wrong color!");
               return;
            }
            Destroy(other.gameObject);
            FoodCount += 1 + multiplier;
            EAddFood?.Invoke(FoodCount.ToString());
            AddPart?.Invoke();
            AudioController.PlaySound(Resources.Load("Sounds/Eat") as AudioClip);
         }
         else if (other.CompareTag("Crystal"))
         {
            if(isStartTimer == false)
               isStartTimer = true;
            Destroy(other.gameObject);
            TotalCrystalCount += 1 + multiplier;
            currentCrystalCount ++;
            EAddCrystal?.Invoke(TotalCrystalCount.ToString());
            AudioController.PlaySound(Resources.Load("Sounds/Crystal") as AudioClip);
         }
      }

      public void FeverEating(Component other)
      {
         if (other.CompareTag("Point")) 
            return;
         if (other.CompareTag("Food"))
         {
            FoodCount += 1 + multiplier;
            EAddFood?.Invoke(FoodCount.ToString());
            AddPart?.Invoke();
         }
         Destroy(other.gameObject);
         AudioController.PlaySound(Resources.Load("Sounds/Eat") as AudioClip);
      }

      private void FeverTimer()
      {
         timer += Time.deltaTime;
         var seconds = (int) (timer % 60);
         if (seconds < 2 && currentCrystalCount >= 4)
         {
            TotalCrystalCount = 0;
            timer = 0;
            currentCrystalCount = 0;
            isStartTimer = false;
            IsFever = true;
            speedEffect.Play();
            StartCoroutine(EndFever());
            EAddCrystal?.Invoke(TotalCrystalCount.ToString());
            
            AudioController.StopPlaying();
            AudioController.PlaySound(Resources.Load("Sounds/Fever") as AudioClip);
         }
         else if(seconds > 2)
         {
            timer = 0;
            currentCrystalCount = 0;
            isStartTimer = false;
         }
      }
      
      private IEnumerator EndFever()
      {
         yield return new WaitForSeconds(5f);
         speedEffect.Stop();
         IsFever = false;
      }
      
      public void ChangeSnakeColor(Colors colorName)
      {
         currentColor = colorName;
         EChangeColor?.Invoke(currentColor.ToString());
      }
      
      public void Death(string reason)
      {
         GameSate.CurrentState = GameSate.State.Dead;
         
         SaveCurrentScore();
         UpdateScoreBoard();
         
         PlayerPrefs.DeleteKey("FoodCount");
         PlayerPrefs.DeleteKey("CrystalCount");
         PlayerPrefs.DeleteKey("Multiplier");
         
         AudioController.PlaySound(Resources.Load("Sounds/Dead") as AudioClip);
         EMessage?.Invoke("Sorry your lose! " + reason);
         Destroy(transform.parent.gameObject);
      }

      public static void UpdateScoreBoard()
      {
         var newUser = new UserHighScore
         {
            username = "Snake_" + Random.Range(0,100),
            foodScore = PlayerPrefs.GetInt("FoodCount"),
            crystalScore = PlayerPrefs.GetInt("CrystalCount"),
            totalScore = PlayerPrefs.GetInt("FoodCount") + PlayerPrefs.GetInt("CrystalCount") * 2
         };
         ScoreboardController.AddEntry(newUser);
      }
      
      public void SaveCurrentScore()
      {
         var fCount = PlayerPrefs.GetInt("FoodCount");
         var cCount = PlayerPrefs.GetInt("CrystalCount");
         fCount += FoodCount;
         cCount += TotalCrystalCount;
         PlayerPrefs.SetInt("FoodCount", fCount);
         PlayerPrefs.SetInt("CrystalCount", cCount);
         PlayerPrefs.Save();
      }
      
   }
}
